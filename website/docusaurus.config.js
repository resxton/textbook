// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');
const math = require('remark-math');
const katex = require('rehype-katex');


async function createConfig() {
  const mdxMermaid = await import('mdx-mermaid')

  /** @type {import('@docusaurus/types').Config} */
  return {
    title: 'Программирование на основе классов и шаблонов',
    tagline: 'Бакалавриат / 1 курс / 2 семестр (весна)',
    favicon: 'img/favicon.ico',

    // Set the production url of your site here
    url: 'https://cpp2.docs.iu5edu.ru/',
    // Set the /<baseUrl>/ pathname under which your site is served
    // For GitHub pages deployment, it is often '/<projectName>/'
    baseUrl: '/',

    // GitHub pages deployment config.
    // If you aren't using GitHub pages, you don't need these.
    organizationName: 'ИУ5 курсы', // Usually your GitHub org/user name.
    projectName: 'cpp-course-sem2', // Usually your repo name.

    onBrokenLinks: 'throw',
    onBrokenMarkdownLinks: 'warn',

    // Even if you don't use internalization, you can use this field to set useful
    // metadata like html lang. For example, if your site is Chinese, you may want
    // to replace "en" with "zh-Hans".
    i18n: {
      defaultLocale: 'ru',
      locales: ['ru'],
    },

    presets: [
      [
        'classic',
        /** @type {import('@docusaurus/preset-classic').Options} */
        ({
          docs: {
            remarkPlugins: [mdxMermaid.default, math],
            rehypePlugins: [katex],
            sidebarPath: require.resolve('./sidebars.js'),
            // Please change this to your repo.
            // Remove this to remove the "edit this page" links.
            editUrl:
              'https://gitlab.com/iu5edu/cpp-course-sem2/textbook/-/tree/main/website',
          },
          blog: {
            showReadingTime: true,
            // Please change this to your repo.
            // Remove this to remove the "edit this page" links.
            editUrl:
              'https://gitlab.com/iu5edu/cpp-course-sem2/textbook/-/tree/main/website',
          },
          theme: {
            customCss: require.resolve('./src/css/custom.css'),
          },
        }),
      ],
    ],

    themeConfig:
      /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
      ({
        // Replace with your project's social card
        image: 'img/docusaurus-social-card.jpg',
        navbar: {
          title: 'ПКШ',
          logo: {
            alt: 'ПКШ Logo',
            src: 'img/logo.svg',
          },
          items: [
            {
              type: 'doc',
              docId: 'intro',
              position: 'left',
              label: 'Руководство',
            },
            { to: '/blog', label: 'Блог', position: 'left' },
            {
              href: 'https://gitlab.com/iu5edu/cpp-course-sem2',
              label: 'GitLab',
              position: 'right',
            },
          ],
        },
        footer: {
          style: 'dark',
          links: [
            {
              title: 'Учебное пособие',
              items: [
                {
                  label: 'Руководство',
                  to: '/docs/intro',
                },
              ],
            },
            {
              title: 'Сообщество',
              items: [
                {
                  label: 'ИУ5 курсы в ВК',
                  href: 'https://vk.com/iu5edu',
                },
                {
                  label: 'ИУ5 в ВК',
                  href: 'https://vk.com/iu5_official',
                },
              ],
            },
            {
              title: 'Дополнительно',
              items: [
                {
                  label: 'Блог',
                  to: '/blog',
                },
                {
                  label: 'GitLab',
                  href: 'https://gitlab.com/iu5edu/cpp-course-sem2',
                },
              ],
            },
          ],
          copyright: `Атрибуция: ИУ5 курсы. ${new Date().getFullYear()}.<br>Текст доступен по лицензии <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.ru"> Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)</a>, если не указано иного.`,
        },
        prism: {
          theme: lightCodeTheme,
          darkTheme: darkCodeTheme,
        },
      }),
    stylesheets: [
      {
        href: 'https://cdn.jsdelivr.net/npm/katex@0.13.24/dist/katex.min.css',
        type: 'text/css',
        integrity:
          'sha384-odtC+0UGzzFL/6PNoE8rX/SPcQDXBJ+uRepguP4QkPCm2LBxH3FA3y+fKSiJ+AmM',
        crossorigin: 'anonymous',
      },
    ],
  };
}
module.exports = createConfig();
