---
id: homework-intro
slug: /homework
sidebar_position: 1
description: "В данном разделе содержатся описания домашнего задания."
---

# Домашнее задание

:::caution
Данное задание предназначено только для групп `ИУ5-22Б`, `ИУ5-23Б`, `ИУ5-25Б`, `РТ5-21Б`.

Задание может уточняться.

Группы `ИУ5-21Б`, `ИУ5-24Б` получают задание по практике и домашнему заданию от своих ведущих преподавателей.
:::

## Задание

:::danger
Это является прототипом задания и не является финальной версией задания.
:::

Домашнее задание является частью [практики](../practice/intro.md). Для сдачи работы необходимо подготовить отдельный пакет документов.

## Отчетность

:::danger
Информация дополняется
:::
