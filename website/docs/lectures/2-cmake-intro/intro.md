---
id: lecture-cmake-intro
slug: /lectures/cmake-intro
sidebar_position: 1
description: В данном разделе содержатся описания проектно-технологической практики.
--- 

# 2. Системы сборки кроссплатформенного программного обеспечения из исходного кода

## План

1. Этапы создания программы на C++
2. Системы сборки
3. Процедурная и объектная декомпозиция
4. Библиотеки
5. CMake
6. Современные конвейеры сборки программного обеспечивания

## Материал

- [Презентация](https://gitlab.com/iu5edu/cpp-course-sem2/textbook/-/blob/main/src/lectures/cmake-intro/presentation.pdf)
