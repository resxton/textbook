---
id: lab3-intro
slug: /labs/lab3
sidebar_position: 1
---

# 3. Классы. Перегрузка конструкторов и операций

Алиас: `Дроби`.

:::caution
Используется предыдущая версия задания без изменения.

При выполнении задания необходимо учитывать особенности приемки лабораторных работ, о которой рассказано в первой [работе](../lab1/intro.md).
:::

Задание расположено [здесь](https://gitlab.com/iu5edu/cpp-course-sem2/textbook/-/blob/main/src/labs/lab3.doc).
